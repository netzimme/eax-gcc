#!/bin/bash
#
# (c) 2013-2014 Daniel Zimmermann
#



NEEDED_PACKAGES=(             'curl'
                              'i686-w64-mingw32-gcc'
                              'bison'
                              'flex'
                              'gperf'
                              'makeinfo'
                              'libtool'
                              'automake'
                              'gawk'
                              'g++'
                              'pkg-config'
                              'gettext'
                              'help2man'
                )
PACKAGES_INSTALL_INSTRUCTION=('curl' 
                              'mingw-w64' 
                              'bison'
                              'flex'
                              'gperf'
                              'texinfo'
                              'libtool'
                              'automake'
                              'gawk'
                              'build-essential'
                              'pkg-config'
                              'gettext'
                              'help2man'
                             )


PACKAGE_ERROR="0"
PACKAGE_INDEX=0

LIBNCURSES_DEV=`find /usr/include | grep curses`

if [ "${LIBNCURSES_DEV}" == "" ]; then
    echo Please install libncurses-dev! sudo apt-get install libncurses-dev
    PACKAGE_ERROR="1" 
fi

for packages in "${NEEDED_PACKAGES[@]}" ; do
    if ! command -v ${packages} &> /dev/null ; then
        echo Please install ${PACKAGES_INSTALL_INSTRUCTION[${PACKAGE_INDEX}]}! sudo apt-get install ${PACKAGES_INSTALL_INSTRUCTION[${PACKAGE_INDEX}]} 
        PACKAGE_ERROR="1"
    fi
    PACKAGE_INDEX=`expr ${PACKAGE_INDEX} + 1`
done


if [ "$PACKAGE_ERROR" = "0" ]; then
  touch .check_packages
else
  exit 1
fi

