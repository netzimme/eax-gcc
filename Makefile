#
# 
# easy arm x-compiler
# 
# makefile that build a gcc cross compiler for arm
# 
# (c) 2013-2015 spico
#
# Autor : Daniel Zimmermann
#

# dependencies for this makefile
# ----------------------
# sudo apt-get install curl
# sudo apt-get install mingw-w64

# crosstool-ng dependencies
# ----------------------
# sudo apt-get install git
# sudo apt-get install cvs
# sudo apt-get install build-essential
# sudo apt-get install bison
# sudo apt-get install flex
# sudo apt-get install gperf
# sudo apt-get install texinfo
# sudo apt-get install libtool
# sudo apt-get install automake
# sudo apt-get install gawk
# sudo apt-get install libncurses-dev
# sudo apt-get install zlibc zlib1g zlib1g-dev
# sudo apt-get install help2man


# crucifixion-freedom dependencies (mingw32)
# sudo apt-get install libstdc++6:i386
# sudo apt-get install zlib1g:i386

ECHO=echo
RM=rm
MAKE=make
MKDIR=mkdir
TAR=tar
TOUCH=touch
CURL=curl
PATCH=patch
CP=cp
MV=mv
CHMOD=chmod
SED=sed
GIT=git
CD=cd

# config section


EAX_CURRENT_DIR=$(shell pwd)

EAX_COMPANY_NAME=spico

# default to arm
ifeq ($(ARCH),)
EAX_TARGET_ARCH=arm
else
EAX_TARGET_ARCH=$(ARCH)
endif 

ifeq ($(ARCH),nios2)
EAX_TARGET_BAREMETAL=y
endif


EAX_TARGET_SYSTEM=linux

ifeq ($(EAX_TARGET_BAREMETAL),)
EAX_TARGET_CC_PREFIX=$(EAX_TARGET_ARCH)-$(EAX_COMPANY_NAME)-$(EAX_TARGET_SYSTEM)-gnueabi
else
EAX_TARGET_CC_PREFIX=$(EAX_TARGET_ARCH)-$(EAX_COMPANY_NAME)-elf
endif 

EAX_TARGET_BUILDTOOLS_PATH=$(EAX_CURRENT_DIR)/build/toolchain/.build/$(EAX_TARGET_CC_PREFIX)/buildtools
EAX_TARGET_BUILDTOOLS_BIN_PATH=$(EAX_TARGET_BUILDTOOLS_PATH)/bin
EAX_TARGET_CC_PREFIX_PATH=$(EAX_TARGET_BUILDTOOLS_BIN_PATH)/$(EAX_TARGET_CC_PREFIX)

EAX_HOSTTOOLS_INSTALL_PATH=$(EAX_CURRENT_DIR)/hosttools_install

EAX_HOST_MINGW64_NAME=i686-w64-mingw32

EAX_HOST_MINGW32MSVC_NAME=i586-mingw32msvc


EAX_HOST_MINGW_NAME=$(EAX_HOST_MINGW64_NAME)


EAX_PYTHON_PKG_VERSION=2.7.5
EAX_PYTHON_PREFIX_NAME=python-$(EAX_PYTHON_PKG_VERSION)
EAX_CRUCIFIXION_FREEDOM_PREFIX=crucifixion-freedom
EAX_CRUCIFIXION_FREEDOM_BUILD_PATH=$(EAX_CRUCIFIXION_FREEDOM_PREFIX)
EAX_CRUCIFIXION_FREEDOM_SHELL_SCRIPT=$(EAX_CRUCIFIXION_FREEDOM_PREFIX).sh
EAX_PYTHON_BUILDDIR=$(EAX_PYTHON_SOURCE_PATH)/builddir
EAX_PYTHON_INSTALL_PREFIX=python-$(EAX_PYTHON_PKG_VERSION)-gcc
EAX_PYTHON_INSTALL_DIR=$(EAX_PYTHON_BUILDDIR)/install/windows-x86/$(EAX_PYTHON_INSTALL_PREFIX)

EAX_PYTHON_TOOLSCHAINDIR=$(EAX_PYTHON_SOURCE_PATH)/toolchaindir

EAX_AUTOMAKE_PKG_VERSION=1.13.2

EAX_EXPAT_PKG_VERSION=2.1.0

EAX_AUTOMAKE_PKG_NAME=automake-$(EAX_AUTOMAKE_PKG_VERSION)

EAX_EXPAT_PKG_NAME=expat-$(EAX_EXPAT_PKG_VERSION)

EAX_CROSSTOOL_PKG_VERSION=trunk


EAX_CROSSTOOL_PKG_NAME=crosstool-ng-$(EAX_CROSSTOOL_PKG_VERSION)


EAX_CROSSTOOL_CONFIG_FILENAME=eax.$(EAX_TARGET_ARCH).$(EAX_CROSSTOOL_PKG_VERSION).config

EAX_GCC_VERSION=5.3.0
EAX_TARGET_BUILD_SYSTEM=win32

EAX_RELEASE_PKG_NAME=eax-gcc-$(EAX_GCC_VERSION)-$(EAX_TARGET_ARCH)-$(EAX_TARGET_BUILD_SYSTEM)-$(GIT_HASH).tar.bz2

EAX_DOWNLOAD_PREFIX=download
EAX_SOURCE_PREFIX=source
EAX_BINARY_PREFIX=bin
EAX_BUILD_PREFIX=build
EAX_CONFIG_PREFIX=config
EAX_PATCH_PREFIX=patch
EAX_IMAGE_PREFIX=image
EAX_TARGET_PREFIX=target

EAX_DOWNLOAD_PATH=$(EAX_CURRENT_DIR)/$(EAX_DOWNLOAD_PREFIX)
EAX_SOURCE_PATH=$(EAX_CURRENT_DIR)/$(EAX_SOURCE_PREFIX)
EAX_BINARY_PATH=$(EAX_CURRENT_DIR)/$(EAX_BINARY_PREFIX)
EAX_TARGET_PATH=$(EAX_CURRENT_DIR)/$(EAX_TARGET_PREFIX)
EAX_BUILD_PATH=$(EAX_CURRENT_DIR)/$(EAX_BUILD_PREFIX)
EAX_CONFIG_PATH=$(EAX_CURRENT_DIR)/$(EAX_CONFIG_PREFIX)
EAX_IMAGE_PATH=$(EAX_CURRENT_DIR)/$(EAX_IMAGE_PREFIX)
EAX_IMAGE_RELEASE_PATH=$(EAX_CURRENT_DIR)/$(EAX_IMAGE_PREFIX)/x-tools/$(EAX_TARGET_CC_PREFIX)
EAX_IMAGE_RELEASE_SYSROOT_PATH=$(EAX_IMAGE_RELEASE_PATH)/$(EAX_TARGET_CC_PREFIX)/sysroot
EAX_IMAGE_RELEASE_SYSROOT_USR_PATH=$(EAX_IMAGE_RELEASE_SYSROOT_PATH)/usr

EAX_CROSSTOOL_NG_DOWNLOAD_TRUNK_PATH=$(EAX_DOWNLOAD_PATH)/ct_ng_trunk

EAX_CROSSTOOL_NG_DOWNLOAD_PATH=$(EAX_DOWNLOAD_PATH)/$(EAX_CROSSTOOL_PKG_NAME).tar.bz2
EAX_AUTOMAKE_DOWNLOAD_PATH=$(EAX_DOWNLOAD_PATH)/$(EAX_AUTOMAKE_PKG_NAME).tar.xz

EAX_EXPAT_DOWNLOAD_PATH=$(EAX_DOWNLOAD_PATH)/$(EAX_EXPAT_PKG_NAME).tar.gz

EAX_CROSSTOOL_NG_SOURCE_PATH=$(EAX_SOURCE_PATH)/$(EAX_CROSSTOOL_PKG_NAME)
EAX_AUTOMAKE_SOURCE_PATH=$(EAX_SOURCE_PATH)/$(EAX_AUTOMAKE_PKG_NAME)

EAX_EXPAT_SOURCE_PATH=$(EAX_SOURCE_PATH)/$(EAX_EXPAT_PKG_NAME)

EAX_PYTHON_SOURCE_PATH=$(EAX_SOURCE_PATH)/$(EAX_PYTHON_PREFIX_NAME)

EAX_CROSSTOOL_NG_BINARY_PATH=$(EAX_BINARY_PATH)/ct-ng
EAX_CROSSTOOL_NG_BUILD_PATH=$(EAX_BUILD_PATH)/ct-ng
EAX_TOOLCHAIN_NG_BUILD_PATH=$(EAX_BUILD_PATH)/toolchain

EAX_TARGET_DOWNLOAD_PREFIX=$(EAX_TARGET_PREFIX)/$(EAX_DOWNLOAD_PREFIX)
EAX_TARGET_BUILD_PREFIX=$(EAX_TARGET_PREFIX)/$(EAX_BUILD_PREFIX)

EAX_TARGET_DOWNLOAD_PATH=$(EAX_CURRENT_DIR)/$(EAX_TARGET_DOWNLOAD_PREFIX)
EAX_TARGET_BUILD_PATH=$(EAX_CURRENT_DIR)/$(EAX_TARGET_BUILD_PREFIX)
EAX_TARGET_PATH=$(EAX_CURRENT_DIR)/$(EAX_TARGET_PREFIX)


EAX_AUTOMAKE_BUILD_PATH=$(EAX_TARGET_BUILD_PATH)/automake
EAX_EXPAT_BUILD_PATH=$(EAX_TARGET_BUILD_PATH)/expat

EAX_EXPAT_BUILD_TARGET_PATH=$(EAX_TARGET_BUILD_PATH)/expat_target

# make target

$(EAX_BINARY_PATH):
	$(MKDIR) -p $(EAX_BINARY_PATH)

$(EAX_SOURCE_PATH):
	$(MKDIR) -p $(EAX_SOURCE_PATH)

$(EAX_DOWNLOAD_PATH):
	$(MKDIR) -p $(EAX_DOWNLOAD_PATH)

$(EAX_CROSSTOOL_NG_DOWNLOAD_PATH):  $(EAX_DOWNLOAD_PATH)
	$(GIT) clone https://github.com/crosstool-ng/crosstool-ng.git $(EAX_CROSSTOOL_NG_DOWNLOAD_TRUNK_PATH)

$(EAX_CROSSTOOL_NG_SOURCE_PATH): $(EAX_SOURCE_PATH) $(EAX_CROSSTOOL_NG_DOWNLOAD_PATH)
	$(MV) $(EAX_CROSSTOOL_NG_DOWNLOAD_TRUNK_PATH) $(EAX_CROSSTOOL_NG_SOURCE_PATH)
	$(TOUCH) $(EAX_CROSSTOOL_NG_SOURCE_PATH)

$(EAX_CROSSTOOL_NG_BUILD_PATH) : $(EAX_CROSSTOOL_NG_SOURCE_PATH)
	$(MKDIR) -p $(EAX_CROSSTOOL_NG_BUILD_PATH)
	$(CP) -r $(EAX_CROSSTOOL_NG_SOURCE_PATH)/* $(EAX_CROSSTOOL_NG_BUILD_PATH)

$(EAX_TOOLCHAIN_NG_BUILD_PATH): 
	$(MKDIR) -p $(EAX_TOOLCHAIN_NG_BUILD_PATH)

	
patch_ct_ng:
	$(PATCH) -i ../../$(EAX_PATCH_PREFIX)/$(EAX_CROSSTOOL_PKG_NAME)/ct_ng_disable_makelevel.patch $(NG_BUILD_PATH)/Makefile.in

configure_ct-ng: patch_ct_ng
	$(NG_BUILD_PATH)/configure --prefix=$(NG_BINARY_PATH)

bootstrap_ct-ng:
	./bootstrap

make_ct-ng:
	$(MAKE)
	$(MAKE) install

$(EAX_CROSSTOOL_NG_BINARY_PATH) : $(EAX_CROSSTOOL_NG_BUILD_PATH)
	$(MAKE) -f ../../Makefile bootstrap_ct-ng --directory=$(EAX_CROSSTOOL_NG_BUILD_PATH)
	$(MAKE) -f ../../Makefile configure_ct-ng --directory=$(EAX_CROSSTOOL_NG_BUILD_PATH) NG_BUILD_PATH=$(EAX_CROSSTOOL_NG_BUILD_PATH) NG_BINARY_PATH=$(EAX_CROSSTOOL_NG_BINARY_PATH)
	$(MAKE) -f ../../Makefile make_ct-ng --directory=$(EAX_CROSSTOOL_NG_BUILD_PATH)

install_ct_ng: $(EAX_CROSSTOOL_NG_BINARY_PATH)

$(EAX_DOWNLOAD_PATH)/ct_src:
	$(MKDIR) -p $(EAX_DOWNLOAD_PATH)/ct_src


$(EAX_IMAGE_PATH):
	$(MKDIR) -p $(EAX_IMAGE_PATH)


install_all: install_ct_ng $(EAX_DOWNLOAD_PATH)/ct_src $(EAX_IMAGE_PATH)


$(EAX_PYTHON_SOURCE_PATH): $(EAX_SOURCE_PATH)
	$(MKDIR) $(EAX_PYTHON_SOURCE_PATH)


$(EAX_SOURCE_PATH)/gdb-wrapper: $(EAX_SOURCE_PATH)
	$(MKDIR) -p $(EAX_SOURCE_PATH)/gdb-wrapper


$(EAX_SOURCE_PATH)/gdb-wrapper/gdb.exe: $(EAX_SOURCE_PATH)/gdb-wrapper
	$(CP) $(EAX_PATCH_PREFIX)/$(EAX_CRUCIFIXION_FREEDOM_PREFIX)/gdb-wrapper.c $(EAX_SOURCE_PATH)/gdb-wrapper/
	($(CD) $(EAX_SOURCE_PATH)/gdb-wrapper && $(EAX_HOST_MINGW_NAME)-gcc gdb-wrapper.c -o gdb.exe )



ifeq ($(EAX_TARGET_BAREMETAL),)
build_and_install_win32_python: $(EAX_PYTHON_SOURCE_PATH)
	($(CD) $(EAX_PYTHON_SOURCE_PATH) && $(GIT) clone https://github.com/mingwandroid/crucifixion-freedom.git $(EAX_CRUCIFIXION_FREEDOM_BUILD_PATH))
ifeq ($(EAX_PYTHON_PKG_VERSION),2.7.5)
	$(CP) $(EAX_PATCH_PREFIX)/$(EAX_CRUCIFIXION_FREEDOM_PREFIX)/0026-regen-with-bash.patch $(EAX_PYTHON_SOURCE_PATH)/$(EAX_CRUCIFIXION_FREEDOM_BUILD_PATH)/patches/python/$(EAX_PYTHON_PKG_VERSION)/
endif
ifeq ($(EAX_PYTHON_PKG_VERSION),3.3.3)
	$(CP) $(EAX_PATCH_PREFIX)/$(EAX_CRUCIFIXION_FREEDOM_PREFIX)/0026-regen-with-bash.patch $(EAX_PYTHON_SOURCE_PATH)/$(EAX_CRUCIFIXION_FREEDOM_BUILD_PATH)/patches/python/$(EAX_PYTHON_PKG_VERSION)/0555-regen-with-bash.patch
endif
	($(CD) $(EAX_PYTHON_SOURCE_PATH)/$(EAX_CRUCIFIXION_FREEDOM_BUILD_PATH) && ./$(EAX_CRUCIFIXION_FREEDOM_SHELL_SCRIPT) --python-version=$(EAX_PYTHON_PKG_VERSION) --systems=windows-x86 --build-dir=$(EAX_PYTHON_BUILDDIR) --toolchains=$(EAX_PYTHON_TOOLSCHAINDIR) --verbose)
endif

config_ct_ng: $(EAX_TOOLCHAIN_NG_BUILD_PATH)
	$(SED) "s/$(EAX_HOST_MINGW32MSVC_NAME)/$(EAX_HOST_MINGW_NAME)/g" $(EAX_CONFIG_PATH)/$(EAX_CROSSTOOL_CONFIG_FILENAME) > $(EAX_CONFIG_PATH)/$(EAX_CROSSTOOL_CONFIG_FILENAME).tmp
	$(CP) $(EAX_CONFIG_PATH)/$(EAX_CROSSTOOL_CONFIG_FILENAME).tmp $(EAX_TOOLCHAIN_NG_BUILD_PATH)/.config
ifeq ($(EAX_TARGET_BAREMETAL),)

endif

config_all: install_all
	$(MAKE) config_ct_ng 


ifeq ($(EAX_TARGET_BAREMETAL),)
patch_ct_ng_for_python:
	$(SED) "s@--with-python=/usr/bin@--with-python=$(EAX_PYTHON_INSTALL_DIR)/bin/python-config.sh@g" $(EAX_CONFIG_PATH)/$(EAX_CROSSTOOL_CONFIG_FILENAME) > $(EAX_CONFIG_PATH)/$(EAX_CROSSTOOL_CONFIG_FILENAME).tmp
	$(CP) $(EAX_CONFIG_PATH)/$(EAX_CROSSTOOL_CONFIG_FILENAME).tmp $(EAX_TOOLCHAIN_NG_BUILD_PATH)/.config
endif	

build_ct_ng_int:
ifeq ($(EAX_TARGET_BAREMETAL),)
	$(MAKE) patch_ct_ng_for_python --directory=../..
endif
ifeq ($(ct_ng_with_menuconfig),1)
	$(NG_BINARY_PATH)/bin/ct-ng menuconfig
endif
ifeq ($(EAX_TARGET_BAREMETAL),)
	$(MAKE) build_and_install_win32_python --directory=../..
endif
	$(NG_BINARY_PATH)/bin/ct-ng build NG_DOWNLOAD_PATH=$(NG_DOWNLOAD_PATH) NG_IMAGE_PATH=$(NG_IMAGE_PATH)


# build automake 1.13 for host

$(EAX_AUTOMAKE_DOWNLOAD_PATH): $(EAX_DOWNLOAD_PATH)
	$(CURL) -L ftp://ftp.gnu.org/gnu/automake/$(EAX_AUTOMAKE_PKG_NAME).tar.gz -o $(EAX_AUTOMAKE_DOWNLOAD_PATH)

$(EAX_AUTOMAKE_SOURCE_PATH): $(EAX_SOURCE_PATH) $(EAX_AUTOMAKE_DOWNLOAD_PATH)
	$(TAR) -xf $(EAX_AUTOMAKE_DOWNLOAD_PATH) --directory=$(EAX_SOURCE_PATH)
	$(TOUCH) $(EAX_AUTOMAKE_SOURCE_PATH)

$(EAX_AUTOMAKE_BUILD_PATH): $(EAX_AUTOMAKE_SOURCE_PATH)
	$(MKDIR) -p $(EAX_AUTOMAKE_BUILD_PATH)

build_host_automake:
	$(AUTOMAKE_SOURCE_PATH)/configure --prefix=$(HOSTTOOLS_INSTALL_PATH)
	$(MAKE)
	$(MAKE) install

build_and_install_automake_to_host: $(EAX_AUTOMAKE_BUILD_PATH)
	$(CHMOD) +w -R $(EAX_IMAGE_RELEASE_PATH)
	$(MAKE) -f ../../../Makefile build_host_automake --directory=$(EAX_AUTOMAKE_BUILD_PATH) AUTOMAKE_SOURCE_PATH=$(EAX_AUTOMAKE_SOURCE_PATH) HOSTTOOLS_INSTALL_PATH=$(EAX_HOSTTOOLS_INSTALL_PATH)
	$(CHMOD) -w -R $(EAX_IMAGE_RELEASE_PATH)
	$(RM) -rf $(EAX_AUTOMAKE_BUILD_PATH)
	$(RM) -rf $(EAX_AUTOMAKE_SOURCE_PATH)

# end


# build expat for host

$(EAX_EXPAT_DOWNLOAD_PATH): $(EAX_DOWNLOAD_PATH)
	$(CURL) -L http://downloads.sourceforge.net/expat/$(EAX_EXPAT_PKG_NAME).tar.gz -o $(EAX_EXPAT_DOWNLOAD_PATH)

$(EAX_EXPAT_SOURCE_PATH): $(EAX_SOURCE_PATH) $(EAX_EXPAT_DOWNLOAD_PATH)
	$(TAR) -xf $(EAX_EXPAT_DOWNLOAD_PATH) --directory=$(EAX_SOURCE_PATH)
	$(TOUCH) $(EAX_EXPAT_SOURCE_PATH)

$(EAX_EXPAT_BUILD_PATH): $(EAX_EXPAT_SOURCE_PATH)
	$(MKDIR) -p $(EAX_EXPAT_BUILD_PATH)

build_host_expat:
	$(EXPAT_SOURCE_PATH)/configure --build=i686-build_pc-linux-gnu --host=$(EAX_HOST_MINGW_NAME) --prefix=$(IMAGE_RELEASE_PATH)
	$(MAKE)
	$(MAKE) install

build_and_install_expat_to_host: $(EAX_EXPAT_BUILD_PATH)
	$(CHMOD) +w -R $(EAX_IMAGE_RELEASE_PATH)
	$(MAKE) -f ../../../Makefile build_host_expat --directory=$(EAX_EXPAT_BUILD_PATH) EXPAT_SOURCE_PATH=$(EAX_EXPAT_SOURCE_PATH) IMAGE_RELEASE_PATH=$(EAX_IMAGE_RELEASE_PATH)
	$(CHMOD) -w -R $(EAX_IMAGE_RELEASE_PATH)
	$(RM) -rf $(EAX_EXPAT_BUILD_PATH)
	$(RM) -rf $(EAX_EXPAT_SOURCE_PATH)

# end expat


build_ct_ng: config_all
	$(MAKE) -f ../../Makefile build_ct_ng_int --directory=$(EAX_TOOLCHAIN_NG_BUILD_PATH) NG_BINARY_PATH=$(EAX_CROSSTOOL_NG_BINARY_PATH) NG_DOWNLOAD_PATH=$(EAX_DOWNLOAD_PATH) NG_IMAGE_PATH=$(EAX_IMAGE_PATH) ct_ng_with_menuconfig=$(with_menuconfig)
ifeq ($(EAX_TARGET_BAREMETAL),)
	$(MAKE) build_and_install_expat_to_host
endif 

.check_packages:
	./check_packages.sh


build_all: .check_packages
	$(MAKE) build_ct_ng

build_all_with_menuconfig:
	$(MAKE) build_ct_ng with_menuconfig=1

make_tar_archiv: $(EAX_SOURCE_PATH)/gdb-wrapper/gdb.exe
	$(CHMOD) -v 755 $(EAX_IMAGE_RELEASE_PATH)
	$(CHMOD) -v 755 -R $(EAX_IMAGE_RELEASE_PATH)
ifeq ($(EAX_TARGET_BAREMETAL),)
	$(CHMOD) -v 755 $(EAX_PYTHON_INSTALL_DIR)
	$(CHMOD) -v 755 -R $(EAX_PYTHON_INSTALL_DIR)
	$(MKDIR) -p $(EAX_IMAGE_RELEASE_PATH)/opt
	$(CP) -r $(EAX_PYTHON_INSTALL_DIR)/* $(EAX_IMAGE_RELEASE_PATH)/opt
	$(MV) $(EAX_IMAGE_RELEASE_PATH)/bin/arm-spico-linux-gnueabi-gdb.exe $(EAX_IMAGE_RELEASE_PATH)/bin/gdborig.exe
	$(CP) $(EAX_SOURCE_PATH)/gdb-wrapper/gdb.exe $(EAX_IMAGE_RELEASE_PATH)/bin/arm-spico-linux-gnueabi-gdb.exe
	if [ -f /usr/i686-w64-mingw32/lib/libwinpthread-1.dll ]; then $(CP) /usr/i686-w64-mingw32/lib/libwinpthread-1.dll $(EAX_IMAGE_RELEASE_PATH)/bin/libwinpthread-1.dll ; fi
	if [ -f $(EAX_IMAGE_RELEASE_PATH)/bin/libwinpthread-1.dll ]; then $(CHMOD) -v 755 $(EAX_IMAGE_RELEASE_PATH)/bin/libwinpthread-1.dll ; fi
	$(MV) $(EAX_IMAGE_RELEASE_PATH)/$(EAX_TARGET_CC_PREFIX)/sysroot/usr/include/linux/netfilter_ipv4/ipt_ECN.h $(EAX_IMAGE_RELEASE_PATH)/$(EAX_TARGET_CC_PREFIX)/sysroot/usr/include/linux/netfilter_ipv4/ipt_ECN.h_
	$(MV) $(EAX_IMAGE_RELEASE_PATH)/$(EAX_TARGET_CC_PREFIX)/sysroot/usr/include/linux/netfilter_ipv4/ipt_TTL.h $(EAX_IMAGE_RELEASE_PATH)/$(EAX_TARGET_CC_PREFIX)/sysroot/usr/include/linux/netfilter_ipv4/ipt_TTL.h_
	$(MV) $(EAX_IMAGE_RELEASE_PATH)/$(EAX_TARGET_CC_PREFIX)/sysroot/usr/include/linux/netfilter_ipv6/ip6t_HL.h $(EAX_IMAGE_RELEASE_PATH)/$(EAX_TARGET_CC_PREFIX)/sysroot/usr/include/linux/netfilter_ipv6/ip6t_HL.h_
	$(MV) $(EAX_IMAGE_RELEASE_PATH)/$(EAX_TARGET_CC_PREFIX)/sysroot/usr/include/linux/netfilter/xt_RATEEST.h $(EAX_IMAGE_RELEASE_PATH)/$(EAX_TARGET_CC_PREFIX)/sysroot/usr/include/linux/netfilter/xt_RATEEST.h_
	$(MV) $(EAX_IMAGE_RELEASE_PATH)/$(EAX_TARGET_CC_PREFIX)/sysroot/usr/include/linux/netfilter/xt_DSCP.h $(EAX_IMAGE_RELEASE_PATH)/$(EAX_TARGET_CC_PREFIX)/sysroot/usr/include/linux/netfilter/xt_DSCP.h_
	$(MV) $(EAX_IMAGE_RELEASE_PATH)/$(EAX_TARGET_CC_PREFIX)/sysroot/usr/include/linux/netfilter/xt_MARK.h $(EAX_IMAGE_RELEASE_PATH)/$(EAX_TARGET_CC_PREFIX)/sysroot/usr/include/linux/netfilter/xt_MARK.h_
	$(MV) $(EAX_IMAGE_RELEASE_PATH)/$(EAX_TARGET_CC_PREFIX)/sysroot/usr/include/linux/netfilter/xt_CONNMARK.h $(EAX_IMAGE_RELEASE_PATH)/$(EAX_TARGET_CC_PREFIX)/sysroot/usr/include/linux/netfilter/xt_CONNMARK.h_
	$(MV) $(EAX_IMAGE_RELEASE_PATH)/$(EAX_TARGET_CC_PREFIX)/sysroot/usr/include/linux/netfilter/xt_TCPMSS.h $(EAX_IMAGE_RELEASE_PATH)/$(EAX_TARGET_CC_PREFIX)/sysroot/usr/include/linux/netfilter/xt_TCPMSS.h_
endif
	-$(TAR) --hard-dereference -j -h -cf $(EAX_RELEASE_PKG_NAME) -C $(EAX_IMAGE_RELEASE_PATH)/ .
	$(CHMOD) -v 644 $(EAX_RELEASE_PKG_NAME)
	$(RM) -rf $(EAX_IMAGE_RELEASE_PATH)

release_json_link:
	$(ECHO)  "{\"release\":["                                                                                              > release.$(EAX_TARGET_ARCH).json
	$(ECHO)      "{"                                                                                                      >> release.$(EAX_TARGET_ARCH).json
	$(ECHO)          "\"name\":\"$(EAX_TARGET_ARCH) gcc $(EAX_GCC_VERSION) for Windows ($(EAX_TARGET_BUILD_SYSTEM))\","   >> release.$(EAX_TARGET_ARCH).json
	$(ECHO)           "\"filename\":\"$(EAX_RELEASE_PKG_NAME)\""                                                          >> release.$(EAX_TARGET_ARCH).json
	$(ECHO)      "}"                                                                                                      >> release.$(EAX_TARGET_ARCH).json
	$(ECHO)  "]}"                                                                                                         >> release.$(EAX_TARGET_ARCH).json
	
release_image:
	$(MAKE) make_tar_archiv	
	$(MAKE) release_json_link

all: build_all

all_with_menuconfig: build_all_with_menuconfig

clean:
	$(RM) -rf $(EAX_SOURCE_PATH)
	$(RM) -rf $(EAX_BUILD_PATH)
	$(RM) -rf $(EAX_BINARY_PATH)
	$(RM) -rf $(EAX_TARGET_PATH)
	$(RM) -rf $(EAX_HOSTTOOLS_INSTALL_PATH)
	$(RM) -rf $(EAX_CROSSTOOL_NG_DOWNLOAD_TRUNK_PATH)


cleanall: clean
	$(RM) -rf $(EAX_DOWNLOAD_PATH)
	$(RM) -rf $(EAX_IMAGE_PATH)
	$(RM) -f .check_packages


help:
	@$(ECHO) ""
	@$(ECHO) " make <target> <options>"
	@$(ECHO) " target:"
	@$(ECHO) ""
	@$(ECHO) "  all                      build the compile"
	@$(ECHO) "  all_with_menuconfig      start menuconfig on ct-ng bevor build the compile"
	@$(ECHO) "  clean                    remove all files"
	@$(ECHO) "  cleanall                 remove all files incl. downloaded files"
	@$(ECHO) ""
	@$(ECHO) "  options:"
	@$(ECHO) ""
	@$(ECHO) " ARCH="
	@$(ECHO) ""
	@$(ECHO) "  arm                      build a gcc cross compiler for the ARM architecture"
	@$(ECHO) "  nios2                    build a gcc cross compiler for the NIOS2 architecture"
	@$(ECHO) ""
	@$(ECHO) " targetarch="
	@$(ECHO) ""
	@$(ECHO) "  win32_32                 build for win32 32 bit (default)"


.PHONY: all all_with_menuconfig clean cleanall help






